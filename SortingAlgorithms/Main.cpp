#include "VectorAlgorithms.h";
#include <algorithm>
#include <stdlib.h>

using algorithmFP = void(VectorAlgorithms::*)();
void testAlgorithm(VectorAlgorithms, algorithmFP);

int main()
{
	VectorAlgorithms myTest1; 
	std::cout << "Uniformly Random Distributed Vector: \n";
	
	testAlgorithm(myTest1, &VectorAlgorithms::bubbleSort);
	std::cout << std::endl << std::endl;
	testAlgorithm(myTest1, &VectorAlgorithms::insertionSort);
	std::cout << std::endl << std::endl;
	testAlgorithm(myTest1, &VectorAlgorithms::selectionSort);
	std::cout << std::endl << std::endl;
	testAlgorithm(myTest1, &VectorAlgorithms::mergeSort);
	std::cout << std::endl << std::endl;
	
	int arr1[10] = { 93, 10, 51, 95, 24, 94, 14, 49, 88, 56 };

	VectorAlgorithms myTest2(arr1);
	std::cout << "Assigned C Array to Vector: \n";
	testAlgorithm(myTest2, &VectorAlgorithms::bubbleSort);
	std::cout << std::endl << std::endl;
	testAlgorithm(myTest2, &VectorAlgorithms::insertionSort);
	std::cout << std::endl << std::endl;
	testAlgorithm(myTest2, &VectorAlgorithms::mergeSort);
	std::cout << std::endl << std::endl;
	std::cout << std::endl << std::endl;
	system("pause");
	return 0;
}

void testAlgorithm(VectorAlgorithms myTest, algorithmFP mySortAlgorithm)
{
	std::vector<int> correctSort = myTest.getVector();
	std::sort(correctSort.begin(), correctSort.end(), [](int x, int y) { return x > y; }); // C++ library algorithm sort

	std::cout << "Before sort: " << myTest << std::endl;
	(myTest.*mySortAlgorithm)(); // my implemented sort
	std::cout << "After sort (expected): ";

	for (const auto& i : correctSort)
	{
		std::cout << i << " ";
	}

	std::cout << std::endl << "After sort (actual): "
		<< myTest;
}
