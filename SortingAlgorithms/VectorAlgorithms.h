#pragma once
#ifndef VECTORALGORITHMS_H
#define VECTORALGORITHMS_H
#include <vector>
#include <iostream>
#include <memory>

class VectorAlgorithms
{
	friend std::ostream& operator<<(std::ostream& out, VectorAlgorithms&);
public:
	void printVector() const noexcept;
	VectorAlgorithms();
	VectorAlgorithms(const int [10]);

	void bubbleSort() noexcept;
	void insertionSort() noexcept;
	void selectionSort() noexcept;

	void mergeSort() noexcept;
	void mergeSort(std::vector<int>&, int, int) noexcept;
	void merge(std::vector<int>&, int, int, int) noexcept;

	void swap(int&, int&) const noexcept;
	bool search(int) const noexcept;

	const std::vector<int>& getVector() const noexcept;

private:
	std::vector<int> myVector;

};

#endif
