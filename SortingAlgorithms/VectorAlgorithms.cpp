#include "VectorAlgorithms.h";
#include <random>

std::ostream& operator<< (std::ostream& out, VectorAlgorithms& myObj)
{
	for (auto i : myObj.myVector)
	{
		out << i << " ";
	}
	return out;
}

VectorAlgorithms::VectorAlgorithms()
{
	std::random_device rd;
	std::mt19937 mt(rd());
	std::uniform_int_distribution<> dist(1, 100);
	
	for (int i = 0; i < 10; ++i)
	{
		auto temp = dist(mt);
		while (search(temp))
		{
			temp = dist(mt);
		}
		myVector.emplace_back(temp);
	}
}

VectorAlgorithms::VectorAlgorithms(const int arr1[10])
{
	myVector.assign(arr1, arr1 + 10);
}

void VectorAlgorithms::printVector() const noexcept
{
	for (auto i : myVector)
	{
		std::cout << i << " ";
	}
}

void VectorAlgorithms::bubbleSort() noexcept
{
	std::cout << "-----Bubble Sort-----\n";
	using vectorIter = std::vector<int>::iterator;

	auto vSize = myVector.size();
	auto i = 0;
	auto notSwapped = false;
	while (i < vSize && !notSwapped)
	{
		vectorIter iter = myVector.begin();
		notSwapped = true;
		while (iter != myVector.end() - 1)
		{
			if (iter[0] > iter[1])
			{
				swap(iter[0], iter[1]);
				notSwapped = false;
			}
			++iter;
		}
		++i;
	}
}

void VectorAlgorithms::insertionSort() noexcept
{
	std::cout << "-----Insertion Sort-----\n";
	auto numberElements = myVector.size();
	for (int i = 1; i < numberElements; ++i)
	{
		auto key = myVector[i];
		auto j = i - 1;
		while (j > -1 && myVector[j] < key)
		{
			myVector[j + 1] = myVector[j];
			--j;
		}
		myVector[j + 1] = key;
	}
}

void VectorAlgorithms::selectionSort() noexcept
{
	std::cout << "-----Selection Sort-----\n";
	auto numberElements = myVector.size();
	auto found = false;
	for (auto i = 0; i < numberElements; ++i)
	{
		auto currentIndex = i;
		for (auto j = i + 1; j < numberElements; ++j)
		{
			if (myVector[currentIndex] > myVector[j])
			{
				currentIndex = j;
				found = true;
			}
		}
		if (found)
		{
			swap(myVector[currentIndex], myVector[i]);
		}
	}
}

void VectorAlgorithms::mergeSort() noexcept
{
	std::cout << "-----Merge Sort-----\n";
	mergeSort(myVector, 0, myVector.size() - 1);
}

void VectorAlgorithms::mergeSort(std::vector<int>& arr, int p, int r) noexcept
{
	if (p < r)
	{
		int q = (r + p) / 2;
		mergeSort(arr, p, q);
		mergeSort(arr, q + 1, r);
		merge(arr, p, q, r);
	}
}

void VectorAlgorithms::merge
(std::vector<int>& arr, int p, int q, int r) noexcept
{
	int nOne = q - p + 1;
	int nTwo = r - q;
	std::vector<int> left, right;

	int i = 0;
	for (i; i < nOne; ++i)
	{
		left.push_back(arr[p + i]);
	}

	int j = 0;
	for (j; j < nTwo; ++j)
	{
		right.push_back(arr[q + j + 1]);
	}
	left.push_back(INT_MAX);
	right.push_back(INT_MAX);

	i = 0;
	j = 0;

	for (int k = p; k <= r; ++k)
	{
		if (left[i] <= right[j])
		{
			arr[k] = left[i];
			++i;
		}
		else
		{
			arr[k] = right[j];
			++j;
		}
	}
}

void VectorAlgorithms::swap(int& elementOne, int& elementTwo) const noexcept
{
	auto temp = elementOne;
	elementOne = elementTwo;
	elementTwo = temp;
}

bool VectorAlgorithms::search(int numberFind) const noexcept
{
	std::vector<int>::const_iterator iter = myVector.cbegin();
	while (iter != myVector.cend())
	{
		if (numberFind == *iter)
		{
			return true;
		}
		++iter;
	}
	return false;
}

const std::vector<int>& VectorAlgorithms::getVector() const noexcept
{
	return myVector;
}


